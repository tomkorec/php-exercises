## Cookie lišta + gtag

Jedná se o změnu souborů paint.php, util.php a util_konverze.php.

### paint.php

A). Na řádek 13 místo

```php
 //GoogleString gtag
  $s = VykresliGoogleString();
 echo $s;
```

vložit toto:

```php
//GoogleString gtag
vykresliGoogleTag();
```


B). Jako první položku v body vložit volání `vykresliGoogleTagNoscript()`:

```php
  echo '</head>';
  echo '<body>';
  vykresliGoogleTagNoscript();
```

C). Vložit javascriptový soubor `cookie.js` do hlavičky:

```html
<script src="./cookie.js" defer></script>
```

Klíčové je použití atributu defer

### util.php

Vložit následující kód (přidává dvě nové funkce (`vykresliGoogleTag` a `vykresliGoogleTagNoscript`):

```php
function vykresliGoogleTag()
{
  global $web;
  $gtagIdResult = GetRow('SELECT g_tag_manager as gtm from hsh_weby where id=' . $web);

  if (isset($gtagIdResult['gtm'])) {
    $gtagId = $gtagIdResult['gtm'];

    echo "
			<script>
				var dataLayer = dataLayer || []; // Google Tag Manager
				function gtag() {
					dataLayer.push(arguments)
				}
				
				document.addEventListener('setup:gtag', (e)=> {
					gtag(e.detail.settings);
				});
			</script>
			
			<!-- Google Tag Manager -->
			<script>
				(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
				})(window,document,'script','dataLayer', '$gtagId');
				
			</script>
			<!-- End Google Tag Manager -->
		";

  }
}

function vykresliGoogleTagNoscript()
{
  echo "
		<!-- Google Tag Manager (noscript) -->
			<noscript><iframe src='https://www.googletagmanager.com/ns.html?id=GTM-WP9JW36'
			height='0' width='0' style='display:none;visibility:hidden'></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
	";
}
```

### util_konverze.php

Změnit funkci `draw_GoogleConv()` do následující podoby:

```php
function draw_GoogleConv($label, $cena, $objID)
{
	echo "
        <script>
            gtag('event', 'conversion', {
                  'price': '$cena',
                  'currency': 'CZK',
                  'transaction_id': '$objID'
              });
        </script>
    ";
}
```
