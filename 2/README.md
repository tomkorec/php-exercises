# II. Cvičení
## 1. Pohyby na skladu
Dostaneš pole, které udává množství produktu na skladě každý den za posledních 20 dní. Spočítej průměrnou denní spotřebu produktu za toto období. Toto číslo zaokrouhli dolů.
## 2. Palindrom
Napiš funkci, která pozná, zda je zadané slovo palindromem.
## 3. Morseovka
Napiš kód, který pomocí funkce `toMorse()` text do morseovky.

## 4. Rodné číslo
V aplikaci uživatel zadává rodné číslo. Napiš takovou funkci, která dostane jako argument rodné číslo typu `string` a rozhodne, zda je platné nebo neplatné (vrací bool `true` nebo `false`).

**Bonus:** Vrať datum narození dané osoby.

### Pravidla jsou následující:
- rodné číslo musí být dělitelné 11
- čísel před lomítkem je přesně 6, čísel po lomítku je přesně 4
- číslo lze zadat jak ve formě s lomítkem, tak bez lomítka (12 číslic)

## 5. Číslo účtu
V jedné nakupovací aplikaci se zadává číslo účtu, jenomže poslední dobou chodily samé nesmysly. Napiš takovou funkci, která dostane jako argument číslo účtu typu `string` a rozhodne, zda je platné nebo neplatné (vrací bool `true` nebo `false`).

### Pravidla jsou následující:
- číslo účtu bude ve formátu `<cislo-pred-zavorkou>/<kod-banky>`, např. 111333/2700
    - neuvažujeme předčíslí
- číslo před lomítkem musí mít 2-10 číslic
  číslo za lomítkem má přesně 4 číslice
s
