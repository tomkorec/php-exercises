<?php

$text = "Frodo halted for a moment, looking back. Elrond was in his chair and the fire was on his face like summer-light upon the trees. Near him sat the Lady Arwen. To his surprise Frodo saw that Aragorn stood beside her; his dark cloak was thrown back, and he seemed to be clad in elven-mail, and a star shone on his breast. They spoke together, and then suddenly it seemed to Frodo that Arwen turned towards him, and the light of her eyes fell on him from afar and pierced his heart.";
