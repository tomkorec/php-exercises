# I. Cvičení

## 1. Analýza textu
Spočítej, kolik má zadaný text znaků a následně kolik má slov. Vypiš jej.

Poté vyhledej, kolikrát se v textu vyskytuje jméno  **Frodo**, číslo vypiš a toto jméno v celém textu nahraď slovem **Hobbit**.

Znovu spočítej znaky a slova a vypiš celý text kurzívou.

## 2. Analýza čísla
Ve formuláři nech uživatele napsat číslo. Po odeslání mu vypiš, zda je číslo:

- kladné nebo záporné
- sudé nebo liché
- větší než 100

Dále pro číslo napiš

- jeho druhou mocninu
- vyděl ho 10 a zaokrouhli nahoru, výsledek vypiš

## 3. Výpočet známky
Dostaneš číslo od 1 do 5, je to průměr známek za celé pololetí. Za úkol máš udělit na základě toho čísla známku. Ale pozor:
- na jedničku stačí mít výsledek alespoň `1,6`, na čtyřku ale musí být lepší výsledek než `4,4`.

## 4. Fibonnaci 😬
- Napiš funkci, která vrátí pole Fibonnaciho posloupnosti pro zadané `n`.
- Následně všechny prvky pole sečti.


## 5. Kontrola jména
Nech uživatele napsat do formuláře **oslovení** (např.: "Honzo") a datum narození. Následně mu napiš zprávu, ve které ho oslovíš a sdělíš mu, kdy má narozeniny.

**Bonus:** Vypiš za kolik dní ty narozeniny jsou.

_Rada_: Místo funkce `date()` pro výpočet data zkus použít objekt `DateTime`
